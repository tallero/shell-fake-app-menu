# Shell fake app menu

[![License: GPL v3+](https://img.shields.io/badge/license-GPL%20v3%2B-blue.svg)](http://www.gnu.org/licenses/gpl-3.0) 
[![Python 3.x Support](https://img.shields.io/pypi/pyversions/Django.svg)](https://python.org)

<blink><b>Attention</b></blink>: this plugin is deprecated. For a better alternative, see [focused window indicator](https://gitlab.com/tallero/focused-window-indicator).

*Shell fake app menu* is a python script for [Argos](https://github.com/p-e-w/argos) that lets you re-enable GNOME app menu in GNOME shell top panel when its [deprecation](https://wiki.gnome.org/Design/Whiteboards/AppMenuMigration) will complete.

It uses `xdotool` to get window title and app process; it is refreshed every second to seem like it's part of the desktop. It works only on Xorg because of Wayland lacks the equivalent of `xdotool` for now if I'm correct.

If you think app menu was a good tool to get to understand what window got focus, say what you think on this GNOME shell [issue](https://gitlab.gnome.org/GNOME/gnome-shell/issues/624).

Then if you think javascript wasn't as great as the default (only?) language to write GNOME shell extensions, well... let's press on GNOME Developers to get some python or whatever language bindings!

## Installation

To use *Shell fake app menu* you have to install [Argos](https://extensions.gnome.org/extension/1176/argos/) and `xdotool` on your distribution; then, just put `shell-fake-app-menu.1l.1s.py` in `~/.config/argos`.

## About

This program is licensed under [GNU General Public License v3 or later](https://www.gnu.org/licenses/gpl-3.0.en.html) by [Pellegrino Prevete](http://prevete.ml). If you find this program useful, consider offering me a [beer](https://patreon.com/tallero), a new [computer](https://patreon.com/tallero) or a part time remote [job](mailto:pellegrinoprevete@gmail.com) to help me pay the bills.
