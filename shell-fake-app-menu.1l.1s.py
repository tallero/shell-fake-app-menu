#!/usr/bin/env python3

#    shell-fake-app-menu
#
#    ----------------------------------------------------------------------
#    Copyright © 2018  Pellegrino Prevete
#
#    All rights reserved
#    ----------------------------------------------------------------------
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#


from subprocess import check_output as sh
from gi.repository.Gio import AppInfo

def fake_app_menu():
    """Since the org.Organization.App frenzy, it is very difficult to find application icons without parsing desktop files, so here it is, a manually compiled list of icons associated to processes!!!
    """
    window = sh(['xdotool', 'getwindowfocus'], encoding='UTF-8').split("\n")[0]
    pid = sh(['xdotool', 'getwindowpid', window], encoding='UTF-8').split("\n")[0]
    title = sh(['xdotool', 'getwindowname', window], encoding='UTF-8').split("\n")[0]
    process_name = sh(['cat', '/proc/' + pid + '/comm'], encoding='UTF-8').split("\n")[0]
    apps = [x for x in AppInfo.get_all() if x.get_commandline() != None]
    icon = False
    for x in apps:
        command_line = x.get_commandline().split(" ")
        for arg in command_line:
            if process_name in arg:
                icon = x.get_icon().to_string()
                break
    if not icon:
        if process_name == "gnome-terminal-":
            icon = 'utilities-terminal-symbolic.symbolic'
    return {"title":title, "icon":icon}

app = fake_app_menu()

if __name__ == "__main__":
    print(app['title'] + " | iconName=" + app['icon'])
